const si = require('systeminformation');

async function getCPUData() {

    var cpuData = []

    var data = await si.cpu()
    var speed = await si.cpuCurrentspeed()
    var temp = await si.cpuTemperature()
    var load = await si.currentLoad()

    cpuData.push(data.speed)
    cpuData.push(data.speedmax)
    cpuData.push(data.cores)

    cpuData.push(speed.avg)

    cpuData.push(temp.main)
    cpuData.push(temp.max)

    cpuData.push(load.currentload)

    return cpuData

}

async function getCPUCriticality(currentData, thresholdData) {

    var alerts = []

    if (currentData[0] > thresholdData[0] || currentData[1] > thresholdData[0]) alerts.push(true)
    else alerts.push(false)

    if (currentData[2] > thresholdData[1]) alerts.push(true)
    else alerts.push(false)

    if (currentData[3] > thresholdData[2]) alerts.push(true)
    else alerts.push(false)

    return alerts

}

async function getMemoryData() {

    var memData = []

    var mem = await si.mem()

    memData.push(mem.total)
    memData.push(mem.active)
    memData.push(mem.available)

    console.log(memData)

    return memData

}

async function getMemoryCriticality(data, threshold) {

    var alerts = []

    if (data[0] * 100 / data[1] > threshold[0]) alerts.push(true)
    else alerts.push(false)

    return alerts

}

async function getBatteryData() {

    var batData = []

    var bar = await si.battery()

    batData.push(bar.ischarging)
    batData.push(bar.currentcapacity)

    batData.push(bar.percent)

    return batData

}

async function getBatteryCriticality(data, threshold) {

    var alerts = []

    if (data[0] == false && data[1] < threshold[0]) alerts.push(true)
    else alerts.push(false)

    return alerts

}

async function getSpecificProcess(name) {

    var proc = await si.processLoad(name)

    return proc

}

module.exports = {

    getCPUData,
    getCPUCriticality,
    getMemoryData,
    getMemoryCriticality,
    getBatteryData,
    getBatteryCriticality,
    getSpecificProcess

}
